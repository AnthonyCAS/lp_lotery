#ifndef CLASES_H_INCLUDED
#define CLASES_H_INCLUDED
#include <vector>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <sstream>
#include <fstream>
using namespace std;

//funcion para los aleatorios

string num_aleatorio(int a, int b,int tipo){
    if(tipo==2)
        b++;
    int num=rand() %(b-a) + a;
    string caracter;
    stringstream stream;
    stream << num;
    caracter=stream.str();
    if(tipo==2 && caracter.compare("4")==0)
            caracter="+";
    return caracter;
}

//funcion para calculasr los aciertos
template <typename t>
int caciertos(vector<t> apuesta, vector<t> num_winner,int tipo){
    int naciertos=0;
    for(int u=0; u<apuesta.size();++u){
        for(int i=0;i<num_winner.size();++i){
            bool acierto;
            if(tipo==1)
                acierto = (apuesta.at(u) == num_winner.at(i));
            else{
                string ca = apuesta.at(u);
                acierto = (ca.compare(num_winner.at(i))==0);
            }
            if(acierto){
                    naciertos++;
                    break;
                }
        }
    }
    return naciertos;
}

template <typename t>
int caciertosLoto(vector<t> apuesta, vector<t> num_winner,int tipo){
    int naciertos=0;
    int cont = 0;
    int cont_aciertos=0;
    for(int u=1; u<apuesta.size();++u){
            bool acierto;
            acierto = (apuesta.at(u) == num_winner.at(u-1));
            if(acierto)
                cont_aciertos++;
            if(u%2==0 && cont_aciertos == 2){
                cont_aciertos=0;
                naciertos++;
            }
    }
    return naciertos;
}
template <typename tipo>
class directorio{
    public:
        int num_apuestas;
        vector< vector<tipo> > apuestas;
        vector< tipo > resultado;
    directorio(){
        num_apuestas=0;
    }
    void get_apuestas(){
        for(int u=0;u<apuestas.size();++u){
            for(int j=0;j<apuestas.at(u).size();++j)
                cout<<(apuestas.at(u)).at(j)<<" ";
            cout<<"\n";
        }
    }
    void get_resultados(){
        for(int u=0;u<resultado.size();++u)
                cout<<resultado.at(u)<<" ";

    }
};

struct percents{
    double total,premios,premio5,premio4,premio3;
    double costo_bajo,costo_medio,costo_alto;
};
template <typename tipo>
class lotery{
    private:
        string name;
        int type_aleatorio;
        directorio<tipo> *dir;
        double total_apuestas,total_premios;
        double total_premios_percent;
        double total_percent;
        double premio_5aciertos,premio_4aciertos,premio_3aciertos;
        double tipo_apuesta_bajo,tipo_apuesta_medio,tipo_apuesta_alto;
        int aleatorio_down,aleatorio_up;
    public:
    lotery(){ //t tipo de num aleatorio, 1 solo enteros, 2 enteros y el caracter +
        dir = new directorio<tipo>;
        total_apuestas=total_premios=0;
    }
    void init(string name, percents porcentajes,int t,int a, int b){ //t tipo de num aleatorio, 1 solo enteros, 2 enteros y el caracter +
        type_aleatorio=t;
        total_premios_percent = porcentajes.premios;
        total_percent = porcentajes.total;
        premio_5aciertos = porcentajes.premio5;
        premio_4aciertos = porcentajes.premio4;
        premio_3aciertos = porcentajes.premio3;
        tipo_apuesta_bajo = porcentajes.costo_bajo;
        tipo_apuesta_medio = porcentajes.costo_medio;
        tipo_apuesta_alto = porcentajes.costo_alto;
        aleatorio_up=b;
        aleatorio_down=a;
    }
    void insertar_apuesta(vector<tipo> ap,int type){
        dir->apuestas.push_back(ap);
        switch(type){
            case 1:
                total_apuestas+=tipo_apuesta_bajo;
                break;
            case 2:
                total_apuestas+=tipo_apuesta_medio;
                break;
            case 3:
                total_apuestas+=tipo_apuesta_alto;
                break;
        }
        dir->num_apuestas++;
    }

    string sortear(){
        srand((unsigned)time(NULL));
        //calculo valores para lso premios
        total_premios = total_premios_percent * total_apuestas / total_percent; //es el 32 % del total para premios
        double total5aciertos = total_premios*premio_5aciertos;
        double total4aciertos = total_premios*premio_4aciertos;
        double total3aciertos = total_premios *premio_3aciertos;
        //calculo los nuemros ganadores
        int winner_3aciertos=0;
        int winner_4aciertos=0;
        int winner_5aciertos=0;
        cout <<"\n Mostrando apuestas\n\n";
        dir->get_apuestas();
        //el resultado propuesto apra la loteria quina
        //tipo nganadores[]={"8", "25", "36", "37", "60"};
        //dir->resultado.assign ( nganadores,nganadores+sizeof(nganadores) / sizeof(tipo));

        //el resultado propuesto apra la loteria LOtoGOL
//        tipo nganadores[]={"3","2","+","2","3","3","0","+","2","3"};
//        dir->resultado.assign ( nganadores,nganadores+sizeof(nganadores) / sizeof(tipo));

        //PARA GENERAR MI RESULTADO ALEATORIO
        int range;
        if(type_aleatorio == 2)
            range = 10;
        else
            range=5;
        for(int u=0; u <range ;++u){
                dir->resultado.push_back(num_aleatorio(aleatorio_down,aleatorio_up,type_aleatorio));
        }

        cout <<"\n Mostrando Resultados\n\n";
        dir->get_resultados();
        //calculo a los ganadores

        for(int i=0 ; i < dir->num_apuestas ; ++i){
            int aciertos=0;
            if(type_aleatorio == 2)
                 aciertos = caciertosLoto(dir->apuestas.at(i),dir->resultado,1);
            else
                 aciertos = caciertos(dir->apuestas.at(i),dir->resultado,1);
            if(aciertos == 3)
                winner_3aciertos++;
            if(aciertos == 4)
                winner_4aciertos++;
            if(aciertos == 5)
                winner_5aciertos++;
        }

        cout <<"\n Sorteo Loteria  \n\n";
        cout <<"\n total acumulado: " <<  total_apuestas;
        cout <<"\n total premios: " <<  total_premios;
        cout <<"\n 1� (5 acertos): " << winner_5aciertos << " ganadores, c/u recibe: " <<total5aciertos/winner_5aciertos ;
        cout <<"\n 2� (4 acertos): " << winner_4aciertos << " ganadores, c/u recibe: " <<total4aciertos/winner_4aciertos ;
        cout <<"\n 3� (3 acertos): " << winner_3aciertos << " ganadores, c/u recibe: " <<total3aciertos/winner_3aciertos <<endl;
        return "hola";
    }


};
template <typename tr>
class game{
    typedef typename tr::data_type data_type;
    string name;
    lotery <data_type> loteria;
 public:
     game(string name){
         this->name=name;
     }

    void parse(vector<data_type> &apuesta,string cadena)    {
            int pos=0; string aux;
            size_t x;
            int tam = cadena.length();
            stringstream oss;
            data_type valor;
            //cout<<"\n cadena: "<<cadena<< " tam: "<< tam <<endl;
                x=cadena.find(" ");
                aux=cadena.substr(pos,x-pos);
                //cout << "\n warning pos: " <<pos << ", x:"<<x<<endl;
                //cout << aux <<endl;
                oss<<aux;
                oss>> valor; oss.clear();
                apuesta.push_back(valor);
            while(x!=std::string::npos){
                pos=x+1;
                //cout << "\n warning pos: " <<pos << ", x:"<<x <<endl;
                x=cadena.find(" ",x+2);
                aux = cadena.substr(pos,x-pos);
                //cout << aux <<endl;
                        oss<<aux;
                        oss>> valor;
                        oss.clear();
                apuesta.push_back(valor);
            }


    }

    void read_insertQuina(){
      string text = name+(".txt");
      ifstream read(text.data(), ios::in );
      string fila;
      cout << "\n Leyendo apuestas\n\n";
      while(!read.eof()){
            vector<data_type> apuesta;
            getline(read, fila);
            parse(apuesta,fila);
            cout << fila <<endl;
            if(apuesta.size()>8 || apuesta.size() <4)
                continue;
            loteria.insertar_apuesta(apuesta,apuesta.size()%4);
      }
      read.close();
    }
    void read_insertLoto(){
      string text = name+(".txt");
      ifstream read(text.data(), ios::in );
      string fila;
      cout << "\n Leyendo apuestas\n\n";
      while(!read.eof()){
            vector<data_type> apuesta;
            getline(read, fila);
            parse(apuesta,fila);
            cout << fila <<endl;


            if(apuesta.size()!=11)
                continue;
                    int val;
            if(apuesta.at(0).compare("4")==0)
                val=3;
            if(apuesta.at(0).compare("2")==0)
                val=2;
            if(apuesta.at(0).compare("1")==0)
                val=1;
            loteria.insertar_apuesta(apuesta,val);
      }
      read.close();
    }

    void escribir(string result,string cadena){
      ofstream write(cadena.data(), ios::out );
      write<<result;

      write.close();
    }

     void start_quina(){
        //defino porcentajes
         percents caja;
         caja.total=104.5;
         caja.premios=32.2;
         caja.premio5=0.35;
         caja.premio4=0.25;
         caja.premio3=0.25;
         caja.costo_bajo=0.75;
         caja.costo_medio=3;
         caja.costo_alto=7.5;
         loteria.init(name,caja,1,1,80);//        //1 para solo numeros
         read_insertQuina();
         string result=loteria.sortear();
         escribir(result,"resultadoQuina.txt");
    }

   void start_loto(){

         percents caja;
         caja.total=104.4;
         caja.premios=28;
         caja.premio5=0.4;
         caja.premio4=0.3;
         caja.premio3=0.3;
         caja.costo_bajo=0.5;
         caja.costo_medio=1;
         caja.costo_alto=2;
         loteria.init(name,caja,2,1,4);
         read_insertLoto();
         string result=loteria.sortear();
         escribir(result,"resultadoLoto.txt");

    }

   void start_otro(){
        //aqui codigo para generar mi otra loteria

    }
};


template <typename t>
struct trait {
    typedef t data_type;
};
#endif // CLASES_H_INCLUDED
